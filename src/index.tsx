import './App.css';
import './styles/table.css';
import {ITableParams, Table} from "src/table";
import {mockData} from "src/utils/mockData";

// Привет,
// Сброрка взята со стандартного реакт проекта
// таблица генерируется автоматически, таким образом можно добавлять и удалять поля и инпуты для полей сами сгенерятся
// что можно улучшить
// typescript: для такой генерящейся формы нужно писать довольно таки сложные дженерики, которые я не написал
// так-же можно добавить сортировку на каждое поле
// state.page довольно криво работает, не знаю как нужно было сделать правильно
// довольно неоптимизировани сделано так как при рендере ререндериться вся таблица. в реакте за этим следит алгоритм reconcilation,
// а вот самостоятельно реальзовывать чтото такое я не смогу.
// скорее всего задача предполагала такой подход, а не innerhtml
// let div = document.createElement("div");
// let p = document.createElement("p");
// div.append(p);

const paramsRenderPage:ITableParams = [
  {
    name: 'id',
    weight: 20,
    isAlignment: true,
    typeInput: 'string',
  },
  {
    name: 'name',
    weight: 200,
    isAlignment: false,
    typeInput: 'string',
  },
  {
    name: 'gender',
    weight: 30,
    isAlignment: true,
    typeInput: ['male', 'female'],
  },
  {
    name: 'age',
    weight: 20,
    isAlignment: true,
    typeInput: 'string',
  },
  {
    name: 'survived',
    weight: 30,
    isAlignment: false,
    typeInput: ['true', 'false'],
  },
]


const table = new Table(paramsRenderPage, mockData)

window.onscroll = (e) => {
  if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 100) {
    if(table.state.page * 30 < mockData.length){
      table.changeState((state) => { return {page: state.page + 1}})
    }
  }
};