export const renderRow = (item: any, params: Array<{name: string, weight: number}>) => {
  return `
    <div class="table__row">
      ${params.map((i) => `<div class="table__row-item" style="flex: 1 1 ${i.weight}px">${normalizeValue(item[i.name])}</div>`).join(' ')}
    </div>
  `
}

const normalizeValue = (value: any) => {
  if(typeof value === 'number' && isFinite(value)){
    return Math.round(value)
  }else{
    return value
  }
}