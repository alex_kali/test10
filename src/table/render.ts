import {ITableParams} from "src/table/index";
import {renderRow} from "src/table/row";


export const renderTable = (params: ITableParams, initialData: Array<any>) => {
  const data = initialData
  const render = () => `
    <div class="table__row">
      ${params.map((i) => `<div class="table__row-item" style="flex: 1 1 ${i.weight}px">${i.name}</div>`).join(' ')}
    </div>
    
    ${data.map((i) => renderRow(i,params)).join(' ')}
  `
  
  const element = document.getElementById('table-content') as HTMLElement
  
  element.innerHTML = render()
}