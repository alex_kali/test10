import {ITableParams} from "src/table/index";

export const renderInputs = (params: ITableParams) => {
  let result = ``;
  for(let i of params){
    if(i.typeInput === 'string'){
      result+=`
        <div>
          <div class="table__title">${i.name}:</div>
          <input type="text" name="${i.name}">
        </div>
      `
    }else{
      result+=`
        <div>
          <div class="table__title">${i.name}:</div>
          ${i.typeInput.map((variable) => `
            <input type="radio" id="${i.name}__${variable}" name="${i.name}" value="${variable}">
            <label for="${i.name}__${variable}">${variable}</label>
          `).join(' ')}
        </div>
      `
    }
  }
  
  const element = document.getElementById('search-form')
  if (element){
    element.innerHTML = result
  }
}