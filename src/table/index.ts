import {renderTable} from "src/table/render";
import {renderInputs} from "src/table/renderInputs";
type IState = any

export type ITableParams = Array<{name: string, weight: number, isAlignment: boolean, typeInput: 'string' | Array<string>}>
export class Table {
  params: ITableParams;
  data: Array<any>;
  isAlignment:any = {};
  
  state:IState = {
    page: 1,
  }
  
  changeState = (value: Partial<IState> | ((state: IState) => Partial<IState>) ) => {
    if(typeof (value) === "function"){
      this.state = {...this.state, ...value(this.state)}
    } else {
      this.state = {...this.state, ...value}
    }
    
    this.render()
  }
  constructor(params: ITableParams, data: Array<any>) {
    this.data = data;
    this.params = params;
    const isAlignment:any = {}
    
    params.forEach((value, index) => {
      isAlignment[value.name] = value.isAlignment
    })
    this.isAlignment = isAlignment
    
    renderInputs(params)
    
    const inputs = document.querySelectorAll('#search-form input')
    
    inputs.forEach((e) => {
      e.addEventListener('input', (e) => {
        const newData = {}
        // @ts-ignore
        newData[(e.target as any).name] = (e.target as any).value
        this.changeState(newData)
      })
    })
    
    this.render()
  }
  
  
  private render() {
    let result = this.data
    
    for(let i in this.state){
      if(i !== 'page') {
        if (this.state[i]) {
          if (this.isAlignment[i]) {
            result = result.filter((item) => item[i] == this.state[i])
          } else {
            result = result.filter((item) => `${item[i]}`.toLowerCase().includes(this.state[i].toLowerCase()))
          }
        }
      }
    }
    
    renderTable(this.params, result.slice(0,this.state.page * 30))
  }
}